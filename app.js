/// MAIN APP and CONTOROLLER and some MODEL combined in this file

/////////////////////////////
// MAIN APPLICATION FILE
// 1) connect to db
// 2) set up express (http) server
// 3) require some stuff to make parsing easier.
////////////////////////////

var express = require('express');
var app = express();
var bodyParser = require('body-parser');
var multer = require('multer');

// Tell express where to look for static files
app.use(express.static("client"));
//app.use(express.static("javascripts"));


//IAIN - so does this middle automatically figure out what type 
//  and then only apply itself if it is the type it parses?
// If you don't have this stuff, you will have issues getting req.body, etc
app.use(bodyParser.json()); // for parsing application/json
app.use(bodyParser.urlencoded({ extended: true })); // for parsing application/x-www-form-urlencoded
//urlencoding is when they put in %XX for non-ASCII chars
app.use(multer()); // for parsing multipart/form-data  (not used in this app)

// Make the DB connection
var mongoose = require('mongoose');  
mongoose.connect('mongodb://localhost:27017/', function(error) {
	if (error) {
		console.log(error);
	}
});

////////////////////////////////////////////////////////
// The MODEL in MVC/
// 1) Create Schemas
// 2) Setup any pre-save hooks, virtuals, validations and methods
//////////////////////////////////////////////////////


// Schema definition
var MySchema = mongoose.Schema;

var UserSchema  = new MySchema({
	first_name: String,
	last_name: String,
	email: String
});

// Model definition
var User = mongoose.model('users', UserSchema);

// Remove the test data if already in there
User.remove({first_name:"Mike"} , function(err) {
	if (err) { console.log(err);}
}); 

// Create the test data // 
// This would actually not be in the main app.js file.
// Instead the app.js file would have something like this:
// if(config.seedDB) { require('./config/seed'); }
// The seed.js file would call the User_model.js file to create the User Schema
// Then the file would have something like this:
var myUser = User.create({first_name:"Mike", last_name:"Stefanik"});


////////////////////////////////////////////
// The CONTROLLER in MVC/
///////////////////////////////////////////

app.get('/test', function (req, res) {
		res.send("<a href='/users'>Show Users Here</a>");
	});

	app.get('/users', function(req, res) {
		User.find({}, function(err, docs) {
			if (err) {
				console.log(err);
			}
			res.json(docs);
		});
	});
	app.get('/', function(req, res) {
		 res.sendFile(__dirname + '/client/jumboTron.html');
	});

	// Here we have an incoming data request from the view.
	// This code knows how to query the database and does.
	// Then it gets a response from the db and passes that back to the view.
	app.get("/todos", function (req, res) {
		User.find({}, function (err, toDos) {
			if (err) {console.log(err);}
			res.json(toDos);
		});
	});
	app.post("/addname", function (req, res) {
		//var u2 = User.create({first_name:"Tony", last_name:"Yee"});
		var u3 = User.create(
				{first_name:req.body.first_name, 
				last_name:req.body.last_name});
	});


// Start the express server
app.listen(3000);  


